const express = require('express');
const server = express();
const { PORT } = require('./config');
const { HomeRoutes, QuotesRoutes } = require('./routes');
const { NotFoundMiddleware } = require('./middlewares');

/* Middlewares - bloque de código que se ejecuta entre la petición que hace el usuario 
 hasta que la petición llega al servidor */

// Permite acceder a los recursos de la carpeta public
server.use(express.static('./public'));
// Permite transformar las peticiones post en formato json
server.use(express.json());

server.use('/', HomeRoutes);
server.use('/', QuotesRoutes);
server.use(NotFoundMiddleware);

server.listen(PORT, () => {
    console.log(`Server running for the PORT ${PORT}`)
});